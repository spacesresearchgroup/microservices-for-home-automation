# Linguistic Abstractions for Interoperability of IoT Platforms

*The Internet of Things (IoT) advocates for multi-layered platforms---from edge
devices to Cloud nodes---where each layer adopts its own communication
standards (media and data formats). While this freedom is optimal for in-layer
communication, it puzzles cross-layer integration due to incompatibilities
among standards. Also enforcing a unique communication stack within the same
IoT platform is not a solution, as it leads to the current phenomenon of **IoT
islands**, where disparate platforms hardly interact with each other. In this
paper we tackle the problem of IoT cross-layer and cross-platform integration
following a language-based approach. We build on the Jolie programming
language, which provides uniform linguistic abstractions to exploit
heterogeneous communication stacks, allowing the programmer to specify in a
declarative way the desired stack, and to easily change it, even at runtime.
Jolie currently supports the main technologies from Service-Oriented Computing,
such as TCP/IP, Bluetooth, and RMI at transport level, and HTTP and SOAP at
application level. We integrate in Jolie the two most adopted protocols for IoT
communication, i.e., CoAP and MQTT. We report our experience on a case study on
Cloud-based home automation, and we present high-level concepts valuable both
for the general implementation of interoperable systems and for the development
of other language-based solutions.*

## Introduction

The Internet of Things (IoT) advocates for multi-layered software platforms,
each adopting its own media protocols and data
formats.

The problem of integrating layers of the same IoT platform, as well as
different IoT vertical solutions, involves many levels of the communication
stack, spanning from link-layer communication technologies, such as ZigBee and
WiFi, to application-layer protocols like HTTP,
CoAP, and MQTT, reaching the top-most layers of data-format integration.

Concretely, we fork the Jolie interpreter---written in Java---into a prototype
called JIoT, standing for **Jolie for IoT**.

JIoT supports all the protocols already supported by the Jolie interpreter 
(TCP at the transport level, and protocols such as SOAP, RMI and HTTP at the application level), 
while adding the application-level protocols for IoT, namely CoAP 
(and, as a consequence, UDP at the transport level) and MQTT.
Notably, when the application protocol supports different
representation formats (such as JSON, XML, etc.) of the message payload, as in
the case of HTTP and CoAP, JIoT, like Jolie, can automatically marshal and un-marshal data
as required.


### Contribution

To substantiate the effectiveness of our language-based approach to IoT
integration, we add to Jolie support for the main communication stacks used in the
IoT setting. Concretely, the added contribution of JIoT with respect to Jolie
is the integration of two application protocols relevant in the IoT scenario,
namely CoAP and MQTT.
Notably, in JIoT the usage of such protocols is supported by the same
linguistic abstractions that Jolie uses for SOAs protocols such as HTTP and
SOAP.

Even if Jolie provides support for the integration of new protocols, when set
in the context of IoT technology, the task is non trivial. Indeed, all the
protocols previously supported by Jolie exploit the same internal interface,
based on two assumptions: *i*) the usage of underlying technologies that
ensure reliable communications and *ii*) a point-to-point communication
pattern.

However, both these assumptions fail when considering the two IoT technologies
we integrate:

- CoAP communications can be unreliable since they are based on UDP
connectionless datagrams. CoAP provides options for reliable communications,
however these are usually disabled in an IoT setting, since battery and
bandwidth preservation is important;

- MQTT communications are based on the publish-subscribe paradigm, which
contrasts with the point-to-point communication found in Jolie primitives.
Hence, we need to define a mapping of the general abstractions of the Jolie
language onto the publish-subscribe paradigm, balancing two factors: *i*)
preserving the simplicity of use of the point-to-point communication style and
*ii*) capturing the typical publish-subscribe flow of communications. Such
a mapping is particularly challenging in the case of request-response
communications. Remarkably, the mapping that we present in this work is general
and could be used also in other contexts.

## JIoT: Jolie for IoT

Jolie currently supports some of the main technologies used in SOAs (e.g.
HTTP). However, only a limited amount of IoT devices uses the media and
protocols already supported by Jolie. Indeed, protocols such as
CoAP and MQTT, which are widely used in IoT scenarios, are not implemented in Jolie.
Integrating these protocols, as we have done, is essential in order to allow
Jolie programs to directly interact with the majority of IoT devices. We note
that emerging frameworks for interoperability, such as the Web of
Things, rely on the same protocols we mentioned for IoT, thus JIoT
is also compliant with them.

However there are some challenges linked to the integration of these technologies within Jolie:
- *lossless vs. lossy protocols* --- In SOAs,
  machine-to-machine communication relies on lossless protocols, as there are no
  strict constraints on energy consumption or bandwidth, hence the number of
  message exchanges at the transport level needed for ensuring a message
  delivery is not critical. On the contrary, in IoT networks these
  constraints exist and are important, and the choice of the protocol needs to
  take them into account. Many protocols, and the CoAP application protocol in
  particular, rely on the UDP transport protocol --- a connectionless protocol
  that gives no guarantee on the delivery of messages, but allows one to limit
  message exchanges, as well as energy and bandwidth consumption. Since Jolie assumes
  lossless communications, the inclusion of connectionless protocols in the
  language requires careful handling to prevent misbehaviors;

- *point-to-point vs. publish-subscribe* --- In order
  to provide language constructs that do not depend on the chosen protocol, we
  need to find a uniform setting covering both point-to-point communications,
  such as the ones of HTTP and CoAP, and publish-subscribe communications
  typical of MQTT.\@ Jolie already provides language constructs usable with many
  communication protocols, hence the less disruptive approach is to use the same
  constructs, which are designed for a point-to-point setting, also for MQTT.\@
  This requires to find for each point-to-point construct a corresponding effect
  in the publish-subscribe paradigm, such that typical programming patterns
  produce similar effects in both settings. In this way, one can program a
  unique behavior valid for both point-to-point and publish-subscribe
  scenarios.
\end{itemize} 

JIoT is available [here](http://cs.unibo.it/jolie/jiot.html), 
released under the GPL license. 
The code snippets reported in this paper are based on version 1.0 of JIoT. 
The integration of JIoT into the official code-base of the Jolie language is
ongoing work.
