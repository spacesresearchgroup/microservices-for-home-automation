/*
 * Copyright (C) 2019 <stefanopio.zingaro@unibo.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

type Thing: void {
  .user?: string
  .location?: string
  .properties?: undefined
  .actions?: undefined
  .events?: undefined
  .description?: string
  .id: string
  .name: string
  .type: string
}

interface logicEngineIface {
  OneWay: register( Thing )
}

type EngineRequestType: void {
  .operationName: string
  .deviceRequest?: undefined
}

type EngineResponseType: void {
  .thingResponse: undefined
}

interface controllerIface {
  RequestResponse: engine( EngineRequestType )( EngineResponseType )
}
