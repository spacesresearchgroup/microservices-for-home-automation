/*
 * Copyright (C) 2019 <stefanopio.zingaro@unibo.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

inputPort TemperatureSensor {
  Location: "datagram://127.0.0.1:5684"
  Protocol: coap {
    .osc.get.alias = "/temperature/85"
  }
  RequestResponse: get( void )( int )
}

execution{ concurrent }

init
{
  global.temperature = 19
}

main
{
  get()( global.temperature )
}
