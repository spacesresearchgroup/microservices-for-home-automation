/*
 * Copyright (C) 2019 <stefanopio.zingaro@unibo.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include "reflection.iol"
include "file.iol"

include "../controller.iol"

outputPort Thing {
  Protocol: coap {
    .timeout = 10;
    .osc.get << {
      .osc.get.alias = "/temperature/85",
      .messageType = "CON"
    }
  }
  RequestResponse: get( void )( int )
}

inputPort Controller {
  Location: "socket://127.0.0.1:10004"
  Protocol: http
  Interfaces: controllerIface
}

outputPort LogicEngine {
  Location: "socket://127.0.0.1:8000"
  Protocol: http
  Interfaces: logicEngineIface
}

execution{ concurrent }

embedded { Jolie: "thing.ol" }

init
{
  getServiceDirectory@File( )( dir );
  with( file ){
    .filename = dir + "/" + "descriptor.json" ;
    .format = "json"
  } ;
  readFile@File( file )( thing ) ;
  Thing.location = thing.base ;
  undef( thing.base ) ;
  with( thing ){
    .location = global.inputPorts.Controller.location ;
    .user = "userName"
  } ;
  register@LogicEngine( thing )
}


main
{
  engine( request )( response ) {
    with( invokeRequest ){
      .outputPort = "Thing" ;
      .operation = request.operationName ;
      if( is_defined( request.deviceRequest ) ) {
        .data = request.deviceRequest
      }
    } ;
    invoke@Reflection( invokeRequest )( thingResponse ) ;
    response.thingResponse << thingResponse
 }
}
