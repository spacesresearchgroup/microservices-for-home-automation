/*
 * Copyright (C) 2019 <stefanopio.zingaro@unibo.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

inputPort Thermostat {
  Location: "socket://127.0.0.1:9002"
  Protocol: mqtt {
    .broker = "socket://127.0.0.1:1883";
    .osc.set.alias = "thermostat/93/set"
  }
  OneWay: set( int )
}

execution{ concurrent }

init
{
  global.temperature = 19
}

main
{
  set( request );
  global.temperature = request
}
