/*
 * Copyright (C) 2019 <stefanopio.zingaro@unibo.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include "time.iol"

include "controller.iol"

outputPort Controller {
  Protocol: http
  Interfaces: controllerIface
}

inputPort LogicEngine {
  Location: "socket://localhost:8000"
  Protocol: http
  Interfaces: logicEngineIface
}

execution{ concurrent }

cset {
  user: Thing.user
}

define setTemperature {
  for ( i=0, i<#things, i++ ) {
    if( things[ i ].type == "actuator" && is_defined( things[ i ].properties.temperature ) ) {
      Controller.location = things[ i ].location ;
      request.operationName = "set" ;
      request.deviceRequest = comfortTemperature ;
      engine@Controller( request )( result )
    }
  }
}

define setLight {
  for ( i=0, i<#things, i++ ) {
    if( things[ i ].type == "actuator" && is_defined( things[ i ].properties.light ) ) {
      Controller.location = things[ i ].location ;
      request.operationName = "set" ;
      engine@Controller( request )( result )
    }
  }
}

define getMotion {
  for ( i=0, i<#things, i++ ) {
    if( things[ i ].type == "sensor" && is_defined( things[ i ].properties.motion ) ) {
      Controller.location = things[ i ].location ;
      request.operationName = "get" ;
      engine@Controller( request )( result ) ;
      presence = result.thingResponse || presence
    }
  }
}

define getTemperature {
  sum = 0 | n = 0 ;
  for ( i=0, i<#things, i++ ) {
    if( things[ i ].type == "sensor" && is_defined( things[ i ].properties.temperature ) ) {
      Controller.location = things[ i ].location ;
      request.operationName = "get" ;
      engine@Controller( request )( result ) ;
      sum = sum + result.thingResponse | n++
    }
  } ;
  if( n!=0 ) {
    temperature = sum / n
  }
}

define getLight {
  sum = 0.0 | n = 0 ;
  for ( i=0, i<#things, i++ ) {
    if( things[ i ].type == "sensor" && is_defined( things[ i ].properties.light ) ) {
      Controller.location = things[ i ].location ;
      request.operationName = "get" ;
      engine@Controller( request )( result ) ;
      sum = sum + result.thingResponse | n++
    }
  } ;
  if( n!=0 ) {
     light = sum / n
  }
}

define logic {
  getMotion ;
  if( is_defined( presence ) ) {
    getTemperature ;
    if( presence ) {
      if( is_defined( temperature ) && temperature < 19 ) {
        comfortTemperature = 19 ;
        setTemperature
      } ;
      getLight ;
      if( is_defined( light ) && light < 400 ) {
        setLight
      }
    } else {
      if( is_defined( temperature ) && temperature < 16 ) {
        comfortTemperature = 16 ;
        setTemperature
      };
      setLight
    }
  }
}

main {
  {
    register( thing ) ;
    things[ #things ] << thing
  } ; {
    while( true ) {
      sleep@Time( 5000 )() ;
      logic
    } | while( true ) {
      register( thing ) ;
      things[ #things ] << thing
    }
  }
}
